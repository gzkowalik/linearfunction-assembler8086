; multi-segment executable file template.

data segment
    ; add your data here!
    bufor db 6,?,?,?,?,?,'$'   ;buffor na dane
    bufor_b db 6,?,?,?,?,?,'$'
    
    naglowek_a db 'Wprowadz a z zakresu -20 do 20:',0dh,0ah,'$'
    naglowek_b db 0dh,0ah,'Wprowadz b z zakresu -20 do 20:',0dh,0ah, '$'
    
    a dw ?
    b dw ?
    
    x db 0
    y db 0
    
    root dw 0
    nroot dw 0
    root_ulamek dw 0
    root_ulamek2 dw 0
    
    ulamek_a dw 0
    ulamek_b dw 0 
    
    czy_kropkaA dw 0
    czy_kropkaB dw 0
    
    znak_a dw 0
    znak_b dw 0
    znak_root dw 0
    
    odleglosc_p dw 0
    podzialka dw 1
    
    gora dw 0
    dol dw 0
    
    lewo dw 0
    prawo dw 0
    
    no_root db 0dh,0ah,"Funkcja stala - brak miejsca zerowego. $"
    root_exist db 0dh,0ah,"Miejsce zerowe funkcji to: $"
    many_root db 0dh,0ah,"Nieskonczenie wiele miejsc zerowych. $"
    
    skala db 0dh,0ah,"Skala wykresu: $"
    
    blad db 0dh,0ah,"Wystapil blad...$"
    
ends

stack segment
    dw   128  dup(0)
ends

code segment
wyswietl proc
    mov ah,9
    int 21h
ret
endp

error proc
    lea dx, blad
    call wyswietl
    jmp koniec
ret
endp
 
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax

    ; add your code here
    
    
    
    ;################################################################################################
    ;zapisujemy liczbe a  
    
      
    lea dx, naglowek_a
    mov ah, 9
    int 21h
    
    lea dx, bufor ;wczytywanie znakow do bufora
    mov ah, 10
    int 21h
    
    
    xor cx, cx
    mov cl, ds:[bufor+1]; zaladowanie do cl offsetu bufora w ktorym jest dlugosc wprowadzonego napisu
    mov si, offset bufor ;zaladowanie do rejestru si offsetu bufora
    add si, 2 ;przesuwam do 2 aby dostac sie do znakow zapisanych w buforze
    mov dl, cl ;dlugosc napisu
    
    poprawnoscA:
    mov al, ds:[si] ;zaladuj do al zawartosc rejestru si (przechowuje sie tam znak z bufora)
    
    cmp al, '-'
    je minusA

    cmp al, '.' ;patrzymy czy nie wystapila kropka
    je kropkaA
    jne cyfraA
    
    kropkaA:  ;aby bylo ok, kropka musi byc na pozycji wiekszej niz pierwsza i mniejszej niz ostatnia
    cmp cl,dl ; jesli jest na pozycji pierwszej to jest blad
    je error ;
    cmp cl,1 ; jesli jest na pozycji ostatniej to tez blad
    je error
    ;a jak juz jest na pozycji od drugiej do przeostatniej to pobieramy kolejna cyfre po kropce i zakonczymy ladowanie liczby
    inc si
    mov al, ds:[si] ; nastepna liczba po kropce
    cmp al, '0' ;porownaj znak z kodem ascii zera
    jb error ;jesli mniejsze to blad
    cmp al, '9' ; porownaj znak z kodem ascii dziewiatki
    ja error ; jesli wieksze to blad
    ; jezeli przeszlo to zapisuje wartosc tej cyfry w zmiennej ulamek_a
    mov czy_kropkaA, 1; zapisujemy ze pojawila sie kropka
    sub al, '0'
    mov byte ptr ds:[ulamek_a], al
    jmp liczba_poprawnaA
     
    minusA:
    cmp cl,dl   ;patrzymy czy minus wystepuje na pierwszej pozycji
    je ujemnaA   ; jesli tak, to liczba bedzie ujemna
    jne error   ; jesli nie to mamy blad
     
    ujemnaA:
    cmp cl,1 ; jesli zostal wprowadzony sam minus to nie ma liczby i jest blad
    je error
    mov byte ptr ds:[znak_a], 1 ;jesli jest poprawnie to zapisujemy ze liczba a jest ujemna
    jmp dalejA
    
    cyfraA: ;jesli nie wykryto minusa to sprawdzamy czy znak jest cyfra
    cmp al, '0' ;porownaj znak z kodem ascii zera
    jb error ;jesli mniejsze to blad
    cmp al, '9' ; porownaj znak z kodem ascii dziewiatki
    ja error ; jesli wieksze to blad
    
    dalejA:
    inc si
    loop poprawnoscA
    
    liczba_poprawnaA: ;teraz laduje wartosc do rejestru
    xor ax, ax
    xor cx, cx  ;czyszczenie rejestrow
    xor bx, bx
    mov cl, ds:[bufor+1]; zaladowanie do cl offsetu bufora w ktorym jest dlugosc wprowadzonego napisu
    mov si, offset bufor ;zaladowanie do rejestru si offsetu bufora
    cmp znak_a, 1
    je zapisz_ujemneA
    jne zapisz_dodatnieA
    
    zapisz_ujemneA: ;aby zapisac liczbe ujemna, musze przesunac znak bufora o 1
    add si, 3 ;przesuwam do 3 aby dostac sie do znakow zapisanych w buforze za minusem
    dec cl ;dlugosc wlasciwej liczby = dlugosc - 1
    jmp zapiszA
    zapisz_dodatnieA:
    add si, 2 ;przesuwam do 2 aby dostac sie do znakow zapisanych w buforze, a tu nie ma minusa
    jmp zapiszA
    zapiszA:
    mov al, ds:[si]
    sub al, '0'
    dec cl ;jeden znak wykorzystany
    cmp cl, 0
    je dodaj_po_kropceA
    
    dodajA:  ;petla bedzie tworzyla liczbe, jezeli wprowadzono jedna liczbe to nie wykona sie
    inc si ; przesun o kolejny znak
    mov dl, ds:[si]; zaladuj kolejny znak do rejestru dl
    cmp dl, '.'
    je dodaj_po_kropceA:
    ;a jesli nie ma jeszcze to ukladamy liczbe
    mov bx, 10
    imul bx
    add al, ds:[si]
    sub al, '0'
    loop dodajA
    ;jezeli nie bylo kropki to i tak zwiekszamy liczbe razy 10 i dodajemy 0
    ;tutaj dodajemy to co jest po kropce i dostaniemy liczbe 10 razy wieksza niz mamy, ale to
    ;nie ma znaczenia bo i tak podzielimy potem to przez 10 
    dodaj_po_kropceA: ;tutaj bedziemy mnozyc liczbe przez 10
    mov bx, 10
    imul bx
    cmp czy_kropkaA,1 ;jezeli wystapila kropka to w etykiecie dodajA dodamy wartosc ulamkowa
    je dodaj_ulamekA
    jne znakA
    
    dodaj_ulamekA:
    add ax, ulamek_a ; dodajemy liczbe po kropce
    
    znakA:
    cmp znak_a, 1 ;sprawdzamy znak a, jesli znak_a = 1 to liczba a jest ujemna
    jne zapisz_do_zmiennejA
    neg ax
    
    zapisz_do_zmiennejA:
    mov word ptr ds:[a],ax ; zapisujemy liczbe 10 razy wieksza niz docelowe a
    
    cmp a, 200
    jg error
    cmp a, -200
    jl error
    
    
    ;###############################################################################################
    ;zapisujemy liczbe b
    lea dx, naglowek_b
    mov ah, 9
    int 21h
    
    lea dx, bufor_b ;wczytywanie znakow do bufora
    mov ah, 10
    int 21h
    
    
    xor cx, cx
    mov cl, ds:[bufor_b+1]; zaladowanie do cl offsetu bufora w ktorym jest dlugosc wprowadzonego napisu
    mov si, offset bufor_b ;zaladowanie do rejestru si offsetu bufora
    add si, 2 ;przesuwam do 2 aby dostac sie do znakow zapisanych w buforze
    mov dl, cl ;dlugosc napisu
    
    poprawnoscB:
    mov al, ds:[si] ;zaladuj do al zawartosc rejestru si (przechowuje sie tam znak z bufora)
    
    cmp al, '-'
    je minusB

    cmp al, '.' ;patrzymy czy nie wystapila kropka
    je kropkaB
    jne cyfraB
    
    kropkaB:  ;aby bylo ok, kropka musi byc na pozycji wiekszej niz pierwsza i mniejszej niz ostatnia
    cmp cl,dl ; jesli jest na pozycji pierwszej to jest blad
    je error ;
    cmp cl,1 ; jesli jest na pozycji ostatniej to tez blad
    je error
    ;a jak juz jest na pozycji od drugiej do przeostatniej to pobieramy kolejna cyfre po kropce i zakonczymy ladowanie liczby
    inc si
    mov al, ds:[si] ; nastepna liczba po kropce
    cmp al, '0' ;porownaj znak z kodem ascii zera
    jb error ;jesli mniejsze to blad
    cmp al, '9' ; porownaj znak z kodem ascii dziewiatki
    ja error ; jesli wieksze to blad
    ; jezeli przeszlo to zapisuje wartosc tej cyfry w zmiennej ulamek_a
    mov czy_kropkaB, 1; zapisujemy ze pojawila sie kropka
    sub al, '0'
    mov byte ptr ds:[ulamek_b], al
    jmp liczba_poprawnaB
     
    minusB:
    cmp cl,dl   ;patrzymy czy minus wystepuje na pierwszej pozycji
    je ujemnaB   ; jesli tak, to liczba bedzie ujemna
    jne error   ; jesli nie to mamy blad
     
    ujemnaB:
    cmp cl,1 ; jesli zostal wprowadzony sam minus to nie ma liczby i jest blad
    je error
    mov byte ptr ds:[znak_b], 1 ;jesli jest poprawnie to zapisujemy ze liczba a jest ujemna
    jmp dalejB
    
    cyfraB: ;jesli nie wykryto minusa to sprawdzamy czy znak jest cyfra
    cmp al, '0' ;porownaj znak z kodem ascii zera
    jb error ;jesli mniejsze to blad
    cmp al, '9' ; porownaj znak z kodem ascii dziewiatki
    ja error ; jesli wieksze to blad
    
    dalejB:
    inc si
    loop poprawnoscB
    
    liczba_poprawnaB: ;teraz laduje wartosc do rejestru
    xor ax, ax
    xor cx, cx  ;czyszczenie rejestrow
    xor bx, bx
    mov cl, ds:[bufor_b+1]; zaladowanie do cl offsetu bufora w ktorym jest dlugosc wprowadzonego napisu
    mov si, offset bufor_b ;zaladowanie do rejestru si offsetu bufora
    cmp znak_b, 1
    je zapisz_ujemneB
    jne zapisz_dodatnieB
    
    zapisz_ujemneB: ;aby zapisac liczbe ujemna, musze przesunac znak bufora o 1
    add si, 3 ;przesuwam do 3 aby dostac sie do znakow zapisanych w buforze za minusem
    dec cl ;dlugosc wlasciwej liczby = dlugosc - 1
    jmp zapiszB
    zapisz_dodatnieB:
    add si, 2 ;przesuwam do 2 aby dostac sie do znakow zapisanych w buforze, a tu nie ma minusa
    jmp zapiszB
    zapiszB:
    mov al, ds:[si]
    sub al, '0'
    dec cl ;jeden znak wykorzystany
    cmp cl, 0
    je dodaj_po_kropceB
    
    dodajB:  ;petla bedzie tworzyla liczbe, jezeli wprowadzono jedna liczbe to nie wykona sie
    inc si ; przesun o kolejny znak
    mov dl, ds:[si]; zaladuj kolejny znak do rejestru dl
    cmp dl, '.'
    je dodaj_po_kropceB:
    ;a jesli nie ma jeszcze to ukladamy liczbe
    mov bx, 10
    imul bx
    add al, ds:[si]
    sub al, '0'
    loop dodajB
    ;jezeli nie bylo kropki to i tak zwiekszamy liczbe razy 10 i dodajemy 0
    ;tutaj dodajemy to co jest po kropce i dostaniemy liczbe 10 razy wieksza niz mamy, ale to
    ;nie ma znaczenia bo i tak podzielimy potem to przez 10 
    dodaj_po_kropceB: ;tutaj bedziemy mnozyc liczbe przez 10
    mov bx, 10
    imul bx
    cmp czy_kropkaB,1 ;jezeli wystapila kropka to w etykiecie dodajA dodamy wartosc ulamkowa
    je dodaj_ulamekB
    jne znakB
    
    dodaj_ulamekB:
    add ax, ulamek_b ; dodajemy liczbe po kropce
    
    znakB:
    cmp znak_b, 1 ;sprawdzamy znak a, jesli znak_a = 1 to liczba a jest ujemna
    jne zapisz_do_zmiennejB
    neg ax
    
    zapisz_do_zmiennejB:
    mov word ptr ds:[b],ax ; zapisujemy liczbe 10 razy wieksza niz docelowe b
    
    cmp b, 200
    jg error
    cmp b, -200
    jl error
    
    ;###############################################################################################
    ;wyliczamy miejsce zerowe funkcji
    
    cmp a,0
    je brak_pierwiastka
    jne pierwiastek
    
    brak_pierwiastka: ;jesli a = 0 i b !=0 to nie ma miejsc zerowych
    cmp b, 0    ;jezeli b jest rowne 0 to istnieje nieskoncznenie wiele miejsc zerowych
    je nieskonczenie
    
    add nroot, 1
    jmp skala_wykresu
    
    nieskonczenie:
    add nroot, 2
    jmp skala_wykresu
    
    pierwiastek:
    
    licz_dalej:
    
    mov ax,b
    cwd ;poniewaz dzielna musi byc 32 bitowa, kopiuje najbardziej znaczacy bit ax do dx
    mov bx,a
    idiv bx
    neg ax
    mov byte ptr ds:[root], ax
    cmp dx, 0
    je znak_pierwiastka
    
    
    ;jezeli jest jakas reszta to zamieniam ja dziesietnie
    cmp a, 0
    jg licz
    
    neg a ; liczymy wartosc bezwzgledna z liczby ujemnej
    
    licz:
    
    cmp dx, 0
    jg dodatnie_b
    
    neg dx
    
    dodatnie_b:
    mov ax, dx
    mov bx, 10
    imul bx
    mov bx, a
    idiv bx
    
    mov byte ptr ds:[root_ulamek], ax ;zapisywanie pierwszego miejsca po przecinku
    
    ;drugie miejsce po przecinku
    cmp dx, 0
    je znak_pierwiastka
    
    mov ax, dx
    mov bx, 10
    imul bx
    mov bx, a
    idiv bx
    
    mov byte ptr ds:[root_ulamek2], ax ;zapisywanie pierwszego miejsca po przecinku
    
    znak_pierwiastka:
    
    xor ax,ax
    xor bx,bx
    xor cx,cx
    xor dx,dx
        
    mov ax, znak_a
    mov bx, znak_b
    add ax, bx
    
    cwd
    mov bx, 2
    div bx
    
    cmp dx, 0
    je znak_ujemny
    jne znak_dodatni
    
    
    znak_ujemny:
    cmp root, 0
    jne nie_zero
    cmp root_ulamek, 0
    jne nie_zero
    cmp root_ulamek2, 0
    je znak_dodatni 
    
    nie_zero:
    mov byte ptr ds:[znak_root],1
    jmp skala_wykresu
    
    znak_dodatni:
    mov byte ptr ds:[znak_root],0
    jmp skala_wykresu
    
    ;########################################################################
    
    skala_wykresu:
    ;licze skale wykresu
    
    cmp b, 0  ;jesli b = 0 to podzialka jest = 1
    je wyswietl_dane
    ;wyznaczamy podzialke p = |b/10|
    mov ax, b
    cwd
    mov bx, 10
    idiv bx
    cmp ax, 0
    jnle zapisz_podzialke
   
    neg ax
    
    zapisz_podzialke:
    mov word ptr ds:[podzialka], ax
    
    ;###########################################################
    ;wyswietlamy dane o miejscu zerowym i podzialce
    
    wyswietl_dane:
    
    mov ah, 02h ;przestawiam kursor
    xor bh, bh      
    mov dh, 20 
    mov dl, 0   
    int 10h
    
    ;##############################################################
    ;wyswietlanie jaka wartosc ma pojedyncza kratka
    wyswietl_skale:
    lea dx, skala
    call wyswietl
    
    xor dx, dx
    mov ax, podzialka
    cwd
    mov bx, 10
    div bx
   
    xor bx, bx
    mov bl, dl
    cmp ax, 0
    je skala_drugie_miejsce 
    ;jezeli podzialka > 9
   
    mov dl, al
    add dl, '0'
    xor ax, ax
    mov ah, 2
    int 21h
    
    skala_drugie_miejsce:
    mov dl, bl
    add dl, '0'
    xor ax, ax
    mov ah, 2
    int 21h
    
    ;##################################################################
    ;wyswietlanie miejsca zerowego
    
    cmp nroot, 0
    jne wyjatek
    
    ;klasyczne miejsce zerowe
    
    lea dx, root_exist
    call wyswietl 
    cmp znak_root, 0
    je wyswietl_miejsce_zerowe
    
    xor ax, ax   ;jesli miejsce jest ujemne to trzeba wyswietlic minusa i je zanegowac
    xor dx, dx
    mov dl, '-'
    mov ah, 2
    int 21h
    
    neg root
    
    wyswietl_miejsce_zerowe:
    
    calkowita_pierwsza_cyfra:
    
    xor ax, ax
    xor bx, bx
    xor dx, dx
    
    mov ax, root
    cwd
    mov bx, 10
    div bx
    
    mov bl, dl
   
    cmp ax, 0
    je calkowita_druga_cyfra
    
    mov dl, al
    add dl, '0'
    xor ax, ax
    mov ah, 2
    int 21h
    
    calkowita_druga_cyfra:
    
    mov dl, bl
    add dl, '0'
    xor ax, ax
    mov ah, 2
    int 21h
    
    cmp root_ulamek, 0    ;sprawdzamy czy sa jakies liczby po przecinku do wyswietlenia
    jne dziesietna_pierwsza_cyfra
    cmp root_ulamek2, 0
    je odleglosc
    
    dziesietna_pierwsza_cyfra:
    
    xor ax, ax
    xor dx, dx
    
    mov dl, '.'
    mov ah, 2
    int 21h
    
    mov dx, root_ulamek
    add dl, '0'
    mov ah, 2
    int 21h
    
    
    cmp root_ulamek2, 0
    je odleglosc
    
    mov dx, root_ulamek2
    add dl, '0'
    mov ah, 2
    int 21h
    
    jmp odleglosc
    
    wyjatek: ;kiedy jest 0 albo nieskonczonosc miejsc zerowych
    cmp nroot, 1
    je brak_miejsc_zerowych
    
    lea dx, many_root
    call wyswietl
    jmp odleglosc
    
    brak_miejsc_zerowych:
    
    lea dx, no_root
    call wyswietl
    jmp odleglosc
    

    ;#######################################################################################################
    ;tutaj bedziemy wyznaczac jaka odleglosc jest miedzy odpowiadajacymi sobie punktami
    odleglosc:
    
    cmp a, 0   ;jesli a = 0 to jest to inny przypadek
    je przywroc_a
    
    cmp a,0
    jg znajdz
    
    neg a   ;musimy zanegowac a jesli jest ujemne
    
    znajdz:
    
    cmp a,10   ; jesli a = 1 to bedzie wykres bez przerw
    je przywroc_a 
    
    cmp a,10
    ja wieksze_od_1
    
    mniejsze_od_1: ; odleglosc = 10/a (podloga)
    
    mov ax, 10
    cwd
    mov bx, a
    idiv bx
    
    cmp dx, 0   ;zaokraglanie w dol
    ja zapisz
    
    sub ax, 1
    jmp zapisz
    
    wieksze_od_1: ;odleglosc = a/10 - 1
    mov ax, a
    cwd
    mov bx, 10
    div bx
    
    cmp dx, 0
    jne zapisz
    sub ax, 1
    
    zapisz:
    mov word ptr ds:[odleglosc_p], ax
    
    przywroc_a:    
    ;jezeli zanegowalismy a to trzeba z powrotem do niego wrocic
    cmp znak_a,0  ;jesli znak a jest dodatni to idziemy dalej, a jak nie to przywracamy minusa
    je rysuj_osie
    neg a
    
    ;########################################################################################################################################
    rysuj_osie:
     
    mov cx,21
    mov dl,39 ;wiersz 40
    petla_poziom:
    push cx;poniewaz cx zmieni sie w trakcie petli, trzeba go odlozyc na stos a potem pobrac    
        
    mov ah, 02h
    xor bh, bh ;strona 0
    mov dh, 11 ;wiersz 11
    inc dl ;zwieksz wspolrzedna o 1   
    int 10h
 
    mov ah, 09h;znak z atrybutem
    mov al, '#';symbol do wyswietlenia
    xor bh, bh
    mov bl, 0ah;kolor 
    mov cx, 1;jeden znak do wyswietlenia
    int 10h 
    pop cx
    loop petla_poziom  ; to samo dla osi pionowej
    
    mov cx, 25
    mov dh, -1
    petla_pion:
    push cx    
        
    mov ah, 02h
    xor bh, bh      
    mov dl, 50 ; kolumna 50   
    inc dh    
    int 10h
 
    mov ah, 09h     
    mov al, '#'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h 
    pop cx
    loop petla_pion
    
    
    xor ax,ax
    xor bx,bx
    xor cx,cx
    xor dx,dx
    
    
    ;##########################################################################################
    ;rysowanie wykresu w zaleznosci od odleglosci miedzy kolejnymi punktami
    
    rysowanie_wykresu:
    
    rysuj:
    mov cx, 21;
    
    ;trzy przypadki
    ;1) wykres bez odleglosci
    cmp odleglosc_p, 0
    jne wykres_z_odlegloscia
    
    wykres_bez_odleglosci:
    
    push cx
    sub cx, 11 ; wykres jest od 10 do -10
    mov ax, cx
    mov bx, podzialka ;wykres z podzialka jest od podzialka*10 do - podzialka*10
    imul bx
    mov cx, ax
    mov ax, a
    imul cx
    add ax, b
    cwd
    mov bx, 10 ; w ax bedzie prawdziwe y podzielone przez podzialke
    idiv bx
    cwd
    mov bx, podzialka
    idiv bx
    mov bx, 11  ;translacja wykresu
    sub bx, ax
    mov byte ptr ds:[y], bl
    ;przed narysowaniem trzeba uwzglednic podzialke
   
    xor ax,ax
    xor bx,bx
    xor dx,dx
   
    mov ax, cx
    cwd
    mov bx, podzialka
    idiv bx
    mov cx, ax
    add cx, 50
    mov byte ptr ds:[x], cl
    
    
    mov ah, 02h
    xor bh, bh      
    mov dh, y ;y  
    mov dl, x ;x   
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    pop cx
    loop wykres_bez_odleglosci
    jmp koniec
    
    wykres_z_odlegloscia:
    
    cmp a, 10
    jg rysowanie_duze
    cmp a, -10
    jg rysowanie_male
    
    rysowanie_duze:
    
    xor dx, dx
    mov ax, odleglosc_p      ;na gorze rysujemy odleglosc+1/2 kropek
    cwd
    add ax, 1
    mov bx, 2
    div bx
    mov word ptr ds:[gora], ax
    mov word ptr ds:[dol], ax                 
    
    mov cx, 21
    
    petla_rysowania:
    
    push cx
    sub cx, 11 ; wykres jest od 10 do -10
    mov ax, cx
    mov bx, podzialka ;wykres z podzialka jest od podzialka*10 do - podzialka*10
    imul bx
    mov cx, ax
    mov ax, a
    imul cx
    add ax, b
    cwd
    mov bx, 10 ; w ax bedzie prawdziwe y podzielone przez podzialke
    idiv bx
    cwd
    mov bx, podzialka
    idiv bx
    mov bx, 11  ;translacja wykresu
    sub bx, ax
    mov byte ptr ds:[y], bl
    ;przed narysowaniem trzeba uwzglednic podzialke
   
    xor ax,ax
    xor bx,bx
    xor dx,dx
   
    mov ax, cx
    cwd
    mov bx, podzialka
    idiv bx
    mov cx, ax
    add cx, 50
    mov byte ptr ds:[x], cl
    
    mov ah, 02h
    xor bh, bh      
    mov dh, y ;y  
    mov dl, x ;x   
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    ;#########################################################
    ; u gory narysowalismy prawidlowo, teraz czas narysowac kropki na gorze i na dole
    
    mov cx, gora ;tyle ile kropek trzeba narysowac
    xor ax, ax
    mov dh, y ;zaczynamy od poziomy prawdziwego i bedziemy go zwiekszac
    mov dl, x
    cmp y, 0  ;jesli poza zakresem to nie rysujemy
    jb dalej_petla
    cmp y, 25 ; jesli wewnatrz zakresu to rysujemy
    jb rysuj_gora
    rysuj_gora:
    push cx
    mov ah, 02h
    xor bh, bh      
    dec dh  
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    pop cx
    loop rysuj_gora
    
    ;jezeli dol > 0 to rysujemy dol
    cmp dol, 0
    je dalej_petla
    
    mov cx, dol
    mov dh, y ;zaczynamy od poziomy prawdziwego i bedziemy go zmniejszac
    mov dl, x
    rysuj_dol:
    push cx
    mov ah, 02h
    xor bh, bh      
    inc dh  
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    pop cx
    loop rysuj_dol
           
    dalej_petla:       
    pop cx       
    loop petla_rysowania
    jmp koniec
    
    ;############################################################################
    ;dla |a| < 1 
    rysowanie_male: 
    xor dx, dx
    mov ax, odleglosc_p      ;rysujemy a+1/2 kropek na prawo i lewo
    cwd
    add ax, 1
    mov bx, 2
    div bx
    mov word ptr ds:[lewo], ax
    mov word ptr ds:[prawo], ax 
    
    mov cx, 21
    
    petla_rysowaniaM:
    
    push cx
    sub cx, 11 ; wykres jest od 10 do -10
    mov ax, cx
    mov bx, podzialka ;wykres z podzialka jest od podzialka*10 do - podzialka*10
    imul bx
    mov cx, ax
    mov ax, a
    imul cx
    add ax, b
    cwd
    mov bx, 10 ; w ax bedzie prawdziwe y podzielone przez podzialke
    idiv bx
     
    cwd
    mov bx, podzialka
    idiv bx
    mov bx, 11  ;translacja wykresu
    sub bx, ax
    mov byte ptr ds:[y], bl
    ;przed narysowaniem trzeba uwzglednic podzialke
   
    xor ax,ax
    xor bx,bx
    xor dx,dx
   
    mov ax, cx
    cwd
    mov bx, podzialka
    idiv bx
    mov cx, ax
    add cx, 50
    mov byte ptr ds:[x], cl
    
    mov ah, 02h
    xor bh, bh      
    mov dh, y ;y  
    mov dl, x ;x   
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    ;#####################################################################################
    ;rysujemy teraz kropki na lewo i prawo
    
    mov cx, lewo ;tyle ile kropek trzeba narysowac
    xor ax, ax
    mov dh, y ;zaczynamy od poziomy prawdziwego i bedziemy go zmniejszac
    mov dl, x
    cmp x, 40  ;jesli poza zakresem to nie rysujemy
    jb dalej_petla
    cmp x, 61 ; jesli wewnatrz zakresu to rysujemy
    jb rysuj_lewo
    rysuj_lewo:
    push cx
    mov ah, 02h
    xor bh, bh      
    dec dl  
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    pop cx
    loop rysuj_lewo
    
    ;jezeli prawo > 0 to rysujemy dol
    cmp prawo, 0
    je dalej_petla
    
    mov cx, prawo
    mov dh, y ;zaczynamy od poziomy prawdziwego i bedziemy go zmniejszac
    mov dl, x
    rysuj_prawo:
    push cx
    mov ah, 02h
    xor bh, bh      
    inc dl  
    int 10h
    
    mov ah, 09h     
    mov al, 'o'     
    xor bh, bh
    mov bl, 0ah      
    mov cx, 1       
    int 10h
    
    pop cx
    loop rysuj_prawo
    
    
    dalej_rysowanie:
    
    pop cx
    loop petla_rysowaniaM
    
            
    koniec:    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    mov ax, 4c00h ; exit to operating system.
    int 21h    
ends

end start ; set entry point and stop the assembler.
